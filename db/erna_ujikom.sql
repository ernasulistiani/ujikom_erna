-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 11:51 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erna_ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama_admin` varchar(20) NOT NULL,
  `id_level` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_admin`, `id_level`) VALUES
('', 'ernaadmin', '035b3c6377652bd7d49b5d2e9a53ef40', 'Erna Sulistiani', '1'),
('1', 'admin', 'a207d5459debdcf8e8d82c358511640b', 'Erna', '1');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(12) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(30) NOT NULL,
  `saldo` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `id_tarif` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `username`, `password`, `email`, `nomor_kwh`, `nama_pelanggan`, `saldo`, `alamat`, `id_tarif`) VALUES
('20190324001', 'elsa', '7b8a63bf825624deb7590c563d35dc7e', '', '9980', 'Elsa Saskia', 0, 'Cicurug', 3),
('20190324002', 'aldin', '7b8a63bf825624deb7590c563d35dc7e', '', '7809', 'Aldin Mahpudin', 78750, 'Rawasari', 2),
('20190329002', 'hani', '7b8a63bf825624deb7590c563d35dc7e', '', '4567', 'Nurhanipah', 0, 'Cibinong', 3),
('20190401002', 'dini', '7b8a63bf825624deb7590c563d35dc7e', '', '76565', 'Dini Nursafitri', 0, 'Ciherang', 1),
('20190402003', 'manda', '7b8a63bf825624deb7590c563d35dc7e', '', '98776', 'Amanda Jubed', 220500, 'Semplak', 2),
('20190403001', 'indah', '7b8a63bf825624deb7590c563d35dc7e', '', '99877', 'Indah Fitria', 52500, 'Cipor', 1),
('20190404002', 'raja', '7b8a63bf825624deb7590c563d35dc7e', '', '7676', 'Muhammad Raja', 192500, 'ciomas', 1),
('20190404003', 'erna', '7b8a63bf825624deb7590c563d35dc7e', '', '88776', 'Erna Sulistiani', 0, 'Laladon', 1),
('20190404004', 'ridwan', '7b8a63bf825624deb7590c563d35dc7e', '', '4321', 'Muhammad Ridwan', 70000, 'Dramaga', 2),
('20190405001', 'puspita', '29dd43d39b160725e47a38b510f08a2f', 'mewaaa34@gmail.com', '823746', 'Puspita Wulandari', 0, 'Nangela', 2),
('20190406001', 'sela', 'd2fcd8918101a26b0f9fe1970f22a217', 'sela@gmail.com', '1765', 'Shela Listiani', 491500, 'Laladon', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(12) NOT NULL,
  `id_tagihan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(2) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(20) NOT NULL,
  `denda` int(11) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `id_admin` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_tagihan`, `id_pelanggan`, `tanggal_pembayaran`, `bulan_bayar`, `jumlah_bayar`, `biaya_admin`, `denda`, `total_bayar`, `id_admin`) VALUES
('20190401001', '20190401001', '20190401002', '2019-04-01 10:30:15', '4', 80000, 2500, 0, 82500, 'erna'),
('20190402002', '20190402003', '20190324001', '2019-04-02 16:51:00', '4', 100000, 2500, 0, 102500, 'erna'),
('20190403001', '20190403001', '20190403001', '2019-04-03 11:18:53', '4', 5000, 2500, 0, 7500, 'erna'),
('20190404001', '20190404001', '20190402003', '2019-04-04 13:16:48', '4', 27000, 2500, 29500, 29500, 'erna'),
('20190404002', '20190404002', '20190404002', '2019-04-04 15:20:15', '4', 5000, 2500, 0, 7500, 'erna'),
('20190406001', '20190406001', '20190406001', '2019-04-06 16:31:36', '4', 6000, 2500, 0, 8500, 'erna'),
('20190525002', '20190525001', '20190324002', '2019-05-25 12:27:01', '5', 13500, 2500, 5000, 16000, 'erna');

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan`
--

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggunaan`
--

INSERT INTO `penggunaan` (`id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `meter_awal`, `meter_akhir`) VALUES
('20190329001', '2', '3', 2019, '0', '100'),
('20190401001', '20190401002', '4', 2019, '0', '80'),
('20190401002', '2', '4', 2019, '100', '190'),
('20190402002', '20190324001', '4', 2019, '0', '50'),
('20190403001', '20190403001', '4', 2019, '0', '5'),
('20190404001', '20190402003', '4', 2019, '0', '20'),
('20190404002', '20190404002', '4', 2019, '0', '5'),
('20190406001', '20190406001', '4', 2019, '0', '6'),
('20190425001', '20190324002', '4', 2019, '0', '10'),
('20190525001', '20190324002', '5', 2019, '10', '15'),
('20190625001', '20190324002', '6', 2019, '15', '17'),
('20190725001', '20190324002', '7', 2019, '17', '22');

-- --------------------------------------------------------

--
-- Table structure for table `saldo`
--

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saldo`
--

INSERT INTO `saldo` (`id_saldo`, `username`, `jumlah_isi`, `metode`, `tanggal_pengisian`, `status`) VALUES
('20190330002', 'ridwan', 10000, 'Bank BRI', '2019-03-30 11:03:08', 'TELAH DIVERIFIKASI'),
('20190402007', 'ridwan', 100000, 'Bank BRI', '2019-04-02 17:13:48', 'Menunggu Verifikasi'),
('20190403001', 'ridwan', 50000, 'Bank BRI', '2019-04-03 11:06:05', 'TELAH DIVERIFIKASI'),
('20190403002', 'indah', 10000, 'Bank BRI', '2019-04-03 11:12:27', 'TELAH DIVERIFIKASI'),
('20190403003', 'indah', 50000, 'Bank BRI', '2019-04-03 11:13:18', 'TELAH DIVERIFIKASI'),
('20190403004', 'indah', 50000, 'Bank Mandiri', '2019-04-03 11:14:00', 'Menunggu Verifikasi'),
('20190403005', 'indah', 50000, 'Bank BRI', '2019-04-03 11:16:06', 'Menunggu Verifikasi'),
('20190403006', 'aldin', 20000, 'Bank BRI', '2019-04-03 11:22:38', 'TELAH DIVERIFIKASI'),
('20190404001', 'manda', 50000, 'Bank BRI', '2019-04-04 09:27:01', 'TELAH DIVERIFIKASI'),
('20190404002', 'manda', 200000, 'Bank BRI', '2019-04-04 13:15:53', 'TELAH DIVERIFIKASI'),
('20190404003', 'raja', 200000, 'Bank BRI', '2019-04-04 15:18:59', 'TELAH DIVERIFIKASI'),
('20190406001', 'sela', 500000, 'Bank BRI', '2019-04-06 16:30:48', 'TELAH DIVERIFIKASI'),
('20190425001', 'aldin', 100000, 'Bank BRI', '2019-04-25 11:55:47', 'TELAH DIVERIFIKASI');

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(20) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagihan`
--

INSERT INTO `tagihan` (`id_tagihan`, `id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `jumlah_meter`, `status`) VALUES
('20190329001', '20190329001', '2', '3', 2019, '100', 'Lunas'),
('20190401001', '20190401001', '20190401002', '4', 2019, '80', 'Lunas'),
('20190401002', '20190401002', '2', '4', 2019, '90', 'Lunas'),
('20190402003', '20190402002', '20190324001', '4', 2019, '50', 'Lunas'),
('20190403001', '20190403001', '20190403001', '4', 2019, '5', 'Lunas'),
('20190404001', '20190404001', '20190402003', '4', 2019, '20', 'Lunas'),
('20190404002', '20190404002', '20190404002', '4', 2019, '5', 'Lunas'),
('20190406001', '20190406001', '20190406001', '4', 2019, '6', 'Lunas'),
('20190425001', '20190425001', '20190324002', '4', 2019, '10', 'Lunas'),
('20190525001', '20190525001', '20190324002', '5', 2019, '5', 'Lunas'),
('20190625001', '20190625001', '20190324002', '6', 2019, '2', 'Belum Dibayar'),
('20190725001', '20190725001', '20190324002', '7', 2019, '5', 'Belum Dibayar');

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `daya`, `tarifperkwh`) VALUES
(1, '450', 1000),
(2, '900', 1350),
(3, '1300', 2000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `penggunaan`
--
ALTER TABLE `penggunaan`
  ADD PRIMARY KEY (`id_penggunaan`);

--
-- Indexes for table `saldo`
--
ALTER TABLE `saldo`
  ADD PRIMARY KEY (`id_saldo`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id_tarif` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
