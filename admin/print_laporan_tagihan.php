<?php ob_start(); ?>
<html>
<head>
	<title>Cetak Pdf</title>
</head>
<body>
	<h1 style="text-align: center;">Laporan Tagihan</h1>
	<table border="1" widht="100%" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<th>No</th>
			<th>Id Tagihan</th>
			<th>Bulan</th>
			<th>Tahun</th>
			<th>Jumlah Meter</th>
			<th>Status</th>

		</tr>
		<?php
		include "../koneksi.php";
		$no=1;
		$select=mysqli_query($koneksi,"select * from tagihan");
		while($data=mysqli_fetch_array($select))
		{
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['id_tagihan']; ?></td>
				<td><?php echo $data['bulan']; ?></td>
				<td><?php echo $data['tahun']; ?></td>
				<td><?php echo $data['jumlah_meter']; ?></td>
				<td><?php echo $data['status']; ?></td>


			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Laporan Tagihan.pdf', 'D');
?>
