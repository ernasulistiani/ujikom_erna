<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a  href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a class="active-menu" href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a  href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
           <h4 align="center">Verifikasi Top-Up Saldo User</h4>
           <hr/>
           <div class="panel-body">
            <div class="table-responsive">
              <a href="print_verifikasi.php"><button type="button" class="btn btn-info">Export Pdf</button></a>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                  <tr>
                    <th>ID Saldo</th>
                    <th>Username</th>
                    <th>Nominal Top-Up</th>
                    <th>Metode Pembayaran</th>
                    <th>Tanggal Pengisian</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  include '../koneksi.php';
                  $query_saldo = mysqli_query($koneksi, "select * from saldo");
                  while($saldo = mysqli_fetch_array($query_saldo)){

                    ?>
                    <tr>
                      <td><?php echo $saldo['id_saldo']; ?></td>
                      <td><?php echo $saldo['username']; ?></td>
                      <td><?php echo $saldo['jumlah_isi']; ?></td>
                      <td><?php echo $saldo['metode']; ?></td>
                      <td><?php echo $saldo['tanggal_pengisian']; ?></td>
                      <td><?php echo $saldo['status']; ?></td>
                      <td>
                        <a href="verif.php?id_saldo=<?php echo $saldo['id_saldo']; ?>"><button type="button" class="btn btn-warning"> <span class="glyphicon glyphicon-edit"></span> Verifikasi</button></a>
                        <a href="hapus_verifikasi.php?id_saldo=<?php echo $saldo['id_saldo']; ?>"><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>
                      </td> 
                    </tr>

                  <?php } ?>

                </tbody>
              </table>

            </div>
          </div>

         </div>
       </div>
       <!-- /. ROW  -->
       <hr />

     </div>
     <!-- /. PAGE INNER  -->
   </div>
   <!-- /. PAGE WRAPPER  -->
 </div>
 <!-- /. WRAPPER  -->
 <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
 <!-- JQUERY SCRIPTS -->
 <script src="../assets/js/jquery-1.10.2.js"></script>
 <!-- BOOTSTRAP SCRIPTS -->
 <script src="../assets/js/bootstrap.min.js"></script>
 <!-- METISMENU SCRIPTS -->
 <script src="../assets/js/jquery.metisMenu.js"></script>
 <!-- DATA TABLE SCRIPTS -->
<script src="../assets/js/dataTables/jquery.dataTables.js"></script>
<script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- CUSTOM SCRIPTS -->
<script>
  $(document).ready(function () {
    $('#example').dataTable();
  });
</script>
<script src="../assets/js/custom.js"></script>


</body>
</html>
