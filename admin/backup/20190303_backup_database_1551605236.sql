DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `id_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","admin","admin","indahf945@gmail.com","1");
INSERT INTO admin VALUES("2","pengguna","pengguna","","2");
INSERT INTO admin VALUES("3","bank","bank","","3");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","pengguna");
INSERT INTO level VALUES("3","bank");



DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `no_pelanggan` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nomor_kwh` int(11) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `id_tarif` int(11) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("1","1826781","indah","f3385c508ce54d577fd205a1b2ecdfb7","7221","indah fitria","sindang barang jero","0");
INSERT INTO pelanggan VALUES("4","2782671","hani","0db2131577ea9fde85bf982ac4755a2c","82778","nurhanipah","ciomas","0");
INSERT INTO pelanggan VALUES("5","5","putri","4093fed663717c843bea100d17fb67c8","82178","putri","ciomas","5");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_tagihan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `bulan_bayar` varchar(30) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` varchar(10) NOT NULL,
  `total_bayar` varchar(10) NOT NULL,
  `id_admin` int(11) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("1","2","2","2019-02-25","desember","0","1500","85000","2");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(30) NOT NULL,
  `meter_akhir` varchar(30) NOT NULL,
  PRIMARY KEY (`id_penggunaan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("1","2","februari","2019","100","100");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` int(11) NOT NULL AUTO_INCREMENT,
  `id_penggunaan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `bulan` varchar(30) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("1","2","2","januari","2019","9227","belum bayar");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(11) NOT NULL AUTO_INCREMENT,
  `daya` varchar(30) NOT NULL,
  `tarifperkwh` varchar(30) NOT NULL,
  `beban` varchar(10) NOT NULL,
  `denda` varchar(10) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","51","15000","industri","5%");



