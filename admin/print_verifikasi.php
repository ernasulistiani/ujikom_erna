<?php ob_start(); ?>
<html>
<head>
	<title>Cetak Pdf</title>
</head>
<body>
	<h1 style="text-align: center;">Data Top-Up Saldo User</h1>
	<table border="1" widht="100%" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<th>No</th>
			<th>Id Saldo</th>
			<th>Nominal Top-Up Saldo</th>
			<th>Metode Pembayaran</th>
			<th>Tanggal Pengisian</th>
			<th>Status</th>

		</tr>
		<?php
		include "../koneksi.php";
		$no=1;
		$select=mysqli_query($koneksi,"select * from saldo");
		while($data=mysqli_fetch_array($select))
		{
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['id_saldo']; ?></td>
				<td><?php echo $data['jumlah_isi']; ?></td>
				<td><?php echo $data['metode']; ?></td>
				<td><?php echo $data['tanggal_pengisian']; ?></td>
				<td><?php echo $data['status']; ?></td>

			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Top-Up Saldo.pdf', 'D');
?>
