<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a class="active-menu" href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a  href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a  href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <?php 
      $query_menunggu_verifikasi = mysqli_query($koneksi, "SELECT * FROM saldo WHERE status='Menunggu Verifikasi'");
      $jumlah_verifikasi = mysqli_num_rows($query_menunggu_verifikasi);

      $query_transaksi_saldo = mysqli_query($koneksi, "SELECT * FROM saldo WHERE status='TELAH DIVERIFIKASI'");
      $jumlah_transaksi = mysqli_num_rows($query_transaksi_saldo);

      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan");
      $jumlah_user = mysqli_num_rows($query_user);

      $query_saldo_user = mysqli_query($koneksi, "SELECT SUM(saldo) as saldo FROM pelanggan");
      $jumlah_saldo = mysqli_fetch_array($query_saldo_user);
    ?>
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">   
           <h3 align="center">Admin E-PLN</h3>
           <br/>
           <br/>

           <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">           
              <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                  <i class="fa fa-bell-o"></i>
                </span>
                <div class="text-box" >
                  <p class="main-text"><?php echo $jumlah_verifikasi; ?> New</p>
                  <a href="verifikasi.php"><p class="text-muted">Menunggu Verifikasi Saldo</p></a>
                </div>
              </div>          
              <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                  <i class="fa fa-rocket"></i>
                </span>
                <div class="text-box" >
                  <p class="main-text"><?php echo $jumlah_transaksi; ?>X Dilakukan</p>
                  <a href="verifikasi.php"><p class="text-muted">Transaksi Top-Up Saldo</p></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">           
              <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                  <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                  <p class="main-text"><?php echo $jumlah_user; ?> User</p>
                  <p class="text-muted">User Yang Terdaftar</p>
                </div>
              </div>          
              <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                  <i class="fa fa-usd"></i>
                </span>
                <div class="text-box" >
                  <p class="main-text">Rp.<?php echo $jumlah_saldo['saldo']; ?></p>
                  <p class="text-muted">Saldo User</p>
                </div>
              </div>
            </div>
          </div>



        </div>
      </div>
      <!-- /. ROW  -->
      <hr />

    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>


</body>
</html>
