<?php
include '../koneksi.php'; 
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
  $pelanggan = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

      <div class="row">
        <div class="col-md-12">  
         <h4 align="center">CETAK STRUK</h4>

       </div>
     </div>
     <!-- /. ROW  -->

     <?php
     $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan");
     $pelanggan = mysqli_fetch_array($query_pelanggan);

     $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran");
     $pembayaran = mysqli_fetch_array($query_pembayaran);{
      ?>
<div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              STRUK PEMBELIAN
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <h3 align="center">STRUK PEMBELIAN LISTRIK PRABAYAR</h3><br>
                </div>
                <div align="center">
                  <table>
                    <tr>
                      <td>NOMOR METER</td>
                      <td><?php echo $pelanggan['nomor_kwh']; ?></td>
                      <td>TANGGAL PEMBELIAN</td>
                      <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                    </tr>
                    <tr>
                      <td>ID PELANGGAN</td>
                      <td><?php echo $pelanggan['id_pelanggan']; ?></td>
                      <td>NOMINAL PEMBELIAN</td>
                      <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                    </tr>
                    <tr>
                      <td>NAMA PELANGGAN</td>
                      <td><?php echo $pelanggan['nama_pelanggan']; ?></td>
                      <td>BIAYA ADMIN</td>
                      <td><?php echo $pembayaran['biaya_admin']; ?></td>
                    </tr>
                    <?php
                    $id_tarif = $pelanggan['id_tarif'];
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                    $tarif = mysqli_fetch_array($query_tarif);
                    {
                      ?>
                      <tr>
                        <td>TARIF/DAYA</td>
                        <td><?php echo $tarif['tarifperkwh']; ?>/<?php echo $tarif['daya']; ?></td>
                        <td>BIAYA DENDA</td>
                        <td><?php echo $pembayaran['denda']; ?></td>
                      </tr>
                      <tr>
                        <td>TOTAL PEMBAYARAN</td>
                        <td><?php echo $pembayaran['total_bayar']; ?></td>
                      </tr>
                      <?php
                    }
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
  </div>

<script>
  window.load = print_d();
  function print_d(){
    window.print();
  }
</script>

</body>
</html>