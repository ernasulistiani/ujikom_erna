<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
        <div class="row">
          <div class="col-md-12">
           <h4 align="center">Laporan Pembayaran User</h4>
           <div class="panel-body">
              <table class="table table-bordered table-hover container">
                <thead>
                  <tr>
                    <th>ID Pembayaran</th>
                    <th>Nama Pelanggan</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Bulan Bayar</th>
                    <th>Jumlah Bayar</th>
                    <th>Biaya Admin</th>
                    <th>Total Bayar</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  include '../koneksi.php';
                  $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran");
                  while($pembayaran = mysqli_fetch_array($query_pembayaran)){

                    ?>
                    <tr>
                      <td><?php echo $pembayaran['id_pembayaran']; ?></td>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$pembayaran[id_pelanggan]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <td><?php echo $user['nama_pelanggan']; ?></td>
                      <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                      <td>
                        <?php
                        switch ($pembayaran['bulan_bayar']) {
                          case "1";
                            $bulan_bayar = "Januari";
                          break;
                          case "2";
                            $bulan_bayar = "Februari";
                          break;
                          case "3";
                            $bulan_bayar = "Maret";
                          break;
                          case "4";
                            $bulan_bayar = "April";
                          break;
                          case "5";
                            $bulan_bayar = "Mei";
                          break;
                          case "6";
                            $bulan_bayar = "Juni";
                          break;
                          case "7";
                            $bulan_bayar = "Juli";
                          break;
                          case "8";
                            $bulan_bayar = "Agustus";
                          break;
                          case "9";
                            $bulan_bayar = "September";
                          break;
                          case "10";
                            $bulan_bayar = "Oktober";
                          break;
                          case "11";
                            $bulan_bayar = "November";
                          break;
                          case "12";
                            $bulan_bayar = "Desember";
                          break;
                        }
                        echo $bulan_bayar; ?>
                      </td>
                      <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                      <td><?php echo $pembayaran['biaya_admin']; ?></td>
                      <td><?php echo $pembayaran['total_bayar']; ?></td>
                    </tr>

                  <?php } ?>

                </tbody>
              </table>
          </div>

         </div>
       </div>
       <!-- /. ROW  -->
       

 <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
 <!-- JQUERY SCRIPTS -->
 <script src="../assets/js/jquery-1.10.2.js"></script>
 <!-- BOOTSTRAP SCRIPTS -->
 <script src="../assets/js/bootstrap.min.js"></script>
 <!-- METISMENU SCRIPTS -->
 <script src="../assets/js/jquery.metisMenu.js"></script>
<script src="../assets/js/custom.js"></script>
<script>
	window.load = print_d();
	function print_d(){
		window.print();
	}
</script>
</body>
</html>
