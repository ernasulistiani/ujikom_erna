<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a  href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a  href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a  href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
           <h4 align="center">Laporan Penggunaan User</h4>
           <hr/>
           <div class="panel-body">
            <div class="table-responsive">
              <a href="print_laporan_penggunaan.php"><button type="button" class="btn btn-info">Export Pdf</button></a>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                  <tr>
                    <th>ID Penggunaan</th>
                    <th>Nama Pelanggan</th>
                    <th>Nomor Kwh</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Meter Awal</th>
                    <th>Meter Akhir</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  include '../koneksi.php';
                  $query_penggunaan = mysqli_query($koneksi, "select * from penggunaan");
                  while($penggunaan = mysqli_fetch_array($query_penggunaan)){

                    ?>
                    <tr>
                      <td><?php echo $penggunaan['id_penggunaan']; ?></td>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$penggunaan[id_pelanggan]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <td><?php echo $user['nama_pelanggan']; ?></td>
                      <td><?php echo $user['nomor_kwh']; ?></td>
                      <td>
                        <?php
                        switch ($penggunaan['bulan']) {
                          case "1";
                          $bulan = "Januari";
                          break;
                          case "2";
                          $bulan = "Februari";
                          break;
                          case "3";
                          $bulan = "Maret";
                          break;
                          case "4";
                          $bulan = "April";
                          break;
                          case "5";
                          $bulan = "Mei";
                          break;
                          case "6";
                          $bulan = "Juni";
                          break;
                          case "7";
                          $bulan = "Juli";
                          break;
                          case "8";
                          $bulan = "Agustus";
                          break;
                          case "9";
                          $bulan = "September";
                          break;
                          case "10";
                          $bulan = "Oktober";
                          break;
                          case "11";
                          $bulan = "November";
                          break;
                          case "12";
                          $bulan = "Desember";
                          break;
                        }
                        echo $bulan; ?>
                      </td>
                      <td><?php echo $penggunaan['tahun']; ?></td>
                      <td><?php echo $penggunaan['meter_awal']; ?></td>
                      <td><?php echo $penggunaan['meter_akhir']; ?></td>
                      <td>
                        <a href="hapus_penggunaan.php?id_penggunaan=<?php echo $penggunaan['id_penggunaan']; ?>"><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>
                        <a href="hapus_verifikasi.php?id_saldo=<?php echo $saldo['id_saldo']; ?>"><button type="button" class="btn btn-warning"> <span class="glyphicon glyphicon-print"></span> Cetak</button></a>
                      </td>
                    </tr>

                  <?php } ?>

                </tbody>
              </table>

            </div>
          </div>

        </div>
      </div>
      <!-- /. ROW  -->
      <hr />

    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../assets/js/dataTables/jquery.dataTables.js"></script>
<script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- CUSTOM SCRIPTS -->
<script>
  $(document).ready(function () {
    $('#example').dataTable();
  });
</script>
<script src="../assets/js/custom.js"></script>


</body>
</html>
