<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a  href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a class="active-menu" href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a  href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <?php
    $id_saldo = $_GET['id_saldo'];
    $pilih = mysqli_query($koneksi, "SELECT * FROM saldo WHERE id_saldo='$id_saldo'");
    $data = mysqli_fetch_array($pilih);
    ?>
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
            <h4 align="center">Verifikasi Data</h2><br>
             <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <form action="" method="POST">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                          <input type="text" name="username" class="form-control" value="<?php echo $data['username']; ?>" readonly>
                        </div>
                      </div>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE username='$data[username]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                        <div class="col-sm-9">
                          <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $user['nama_pelanggan']; ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Saldo</label>
                        <div class="col-sm-9">
                          <input type="text" name="jumlah_isi" class="form-control" value="<?php echo $data['jumlah_isi']; ?>" readonly>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-danger" name="verif">Verifikasi</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /. ROW  -->

      <?php
      if (isset($_POST['verif'])){
        $username = $_POST['username'];
        $nama_pelanggan = $_POST['nama_pelanggan'];
        $jumlah_isi = $_POST['jumlah_isi'];
        if ($data['status'] == "TELAH DIVERIFIKASI") {
          echo "<script>window.alert('Status telah diverifikasi')
          window.location='verifikasi.php'</script>";
        } else {
        $saldo = mysqli_query($koneksi,"UPDATE saldo SET username = '$username', jumlah_isi = '$jumlah_isi', status ='TELAH DIVERIFIKASI' WHERE id_saldo='$id_saldo'");
        $pelanggan = mysqli_query($koneksi,"UPDATE pelanggan SET saldo = saldo+'$jumlah_isi' WHERE username='$data[username]'");
        if($saldo AND $pelanggan){
          echo "<script>window.alert('Data Berhasil DiVerifikasi')
          window.location='verifikasi.php'</script>";
        }else{
          echo "Gagal";
        }
      } 
    }
      ?>

    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>


</body>
</html>
