<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>

<?php
$connect = new PDO("mysql:host=localhost;dbname=erna_ujikom", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
  $output = '';
  foreach($_POST["table"] as $table)
  {
    $show_table_query = "SHOW CREATE TABLE " . $table . "";
    $statement = $connect->prepare($show_table_query);
    $statement->execute();
    $show_table_result = $statement->fetchAll();

    foreach($show_table_result as $show_table_row)
    {
     $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
   }
   $select_query = "SELECT * FROM " . $table . "";
   $statement = $connect->prepare($select_query);
   $statement->execute();
   $total_row = $statement->rowCount();

   for($count=0; $count<$total_row; $count++)
   {
     $single_result = $statement->fetch(PDO::FETCH_ASSOC);
     $table_column_array = array_keys($single_result);
     $table_value_array = array_values($single_result);
     $output .= "\nINSERT INTO $table (";
     $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
     $output .= "'" . implode("','", $table_value_array) . "');\n";
   }
 }
 $file_name = 'ujikom_PLN_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
 header('Pragma: public');
 header('Content-Length: ' . filesize($file_name));
 ob_clean();
 flush();
 readfile($file_name);
 unlink($file_name);
}

?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a  href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a  href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a class="active-menu" href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
           <h3 align="center">Backup Database</h3>
           <hr>
           <br />
           <div class="container">
             <form method="post" id="export_form" >
               <h4>Pilih Tabel Untuk Export</h4>
               <?php
               foreach($result as $table)
               {
                ?>
                <div class="checkbox">
                  <label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_erna_ujikom"]; ?>" /> <?php echo $table["Tables_in_erna_ujikom"]; ?></label>
                </div>
                <?php
              }
              ?>
              <div class="form-group">
                <input type="submit" name="submit" id="submit" class="btn btn-danger" value="Export" />
              </div>

              <h3>Backup Database To All</h3>
              <div class="form-group">
                <?php
                error_reporting(0);
                $file=date("Ymd").'_backup_database_'.time().'.sql';
                backup_tables("localhost","root","","ujikom_indah",$file);
                ?>
                <div class="form-group pull-left">
                  <a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="btn btn-primary" >Backup</a>
                </div> 
                <?php
                  /*
                  untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
                  jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
                  */
                  function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
                    $link = mysql_connect($host,$user,$pass);
                    mysql_select_db($name,$link);
                    
                    if($tables == '*'){
                      $tables = array();
                      $result = mysql_query('SHOW TABLES');
                      while($row = mysql_fetch_row($result)){
                        $tables[] = $row[0];
                      }
                    }
                    else{//jika hanya table-table tertentu
                      $tables = is_array($tables) ? $tables : explode(',',$tables);
                    }
                    
                    foreach($tables as $table){
                      $result = mysql_query('SELECT * FROM '.$table);
                      $num_fields = mysql_num_fields($result);

                      $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                      $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                      $return.= "\n\n".$row2[1].";\n\n";
                      
                      for ($i = 0; $i < $num_fields; $i++) {
                        while($row = mysql_fetch_row($result)){
                          //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                          $return.= 'INSERT INTO '.$table.' VALUES(';
                          for($j=0; $j<$num_fields; $j++) {
                            //akan menelusuri setiap baris query didalam
                            $row[$j] = addslashes($row[$j]);
                            $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                            if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                            if ($j<($num_fields-1)) { $return.= ','; }
                          }
                          $return.= ");\n";
                        }
                      }
                      $return.="\n\n\n";
                    }             
                    //simpan file di folder
                    $nama_file;
                    
                    $handle = fopen('backup/'.$nama_file,'w+');
                    fwrite($handle,$return);
                    fclose($handle);
                  }
                  ?>
                </div>
              </form>
            </div>
            <hr>
          </div> 
        </div>
      </div>
    </div>
    <!-- /. ROW  -->

  </div>
  <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<script>
  $(document).ready(function(){
   $('#submit').click(function(){
    var count = 0;
    $('.checkbox_table').each(function(){
     if($(this).is(':checked'))
     {
      count = count + 1;
    }
  });
    if(count > 0)
    {
     $('#export_form').submit();
   }
   else
   {
     alert("Please Select Atleast one table for Export");
     return false;
   }
 });
 });
</script>
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>


</body>
</html>
