<?php 
include '../koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
  header('location:../login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
  $admin = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Admin E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Admin E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="../logout.php" class="btn btn-info square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="../img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $admin['nama_admin'];?></p>
          </li>
          <li>
            <a  href="index.php"><i class="fa fa-home fa-2x"></i> Home</a>
          </li>
          <li>
            <a  href="manage_user.php"><i class="fa fa-user fa-2x"></i>Manage User</a>
          </li>
          <li>
            <a  href="manage_tarif.php"><i class="fa fa-usd fa-2x"></i>Manage Tarif</a>
          </li>
          <li>
            <a  href="verifikasi.php"><i class="fa fa-check fa-2x"></i>Verifikasi</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
              </li>
              <li>
                <a href="laporan_tagihan.php">Laporan Tagihan</a>
              </li>
              <li>
                <a href="laporan_penggunaan.php">Laporan Penggunaan</a>
              </li>
            </ul>
          </li>     
          <li>
            <a  href="backup.php"><i class="fa fa-save fa-2x"></i>Backup</a>
          </li>   
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">
           <h4 align="center">Laporan Pembayaran User</h4>
           <hr/>
           <div class="panel-body">
            <div class="table-responsive">
              <button type="button" class="btn btn-info" onClick="print_d()">Print</button>
              <br>
              <br>
              <table class="table table-striped table-bordered table-hover" id="example">
                <thead>
                  <tr>
                    <th>ID Pembayaran</th>
                    <th>Nama Pelanggan</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Bulan Bayar</th>
                    <th>Jumlah Bayar</th>
                    <th>Biaya Admin</th>
                    <th>Total Bayar</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  include '../koneksi.php';
                  $query_pembayaran = mysqli_query($koneksi, "select * from pembayaran");
                  while($pembayaran = mysqli_fetch_array($query_pembayaran)){

                    ?>
                    <tr>
                      <td><?php echo $pembayaran['id_pembayaran']; ?></td>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$pembayaran[id_pelanggan]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <td><?php echo $user['nama_pelanggan']; ?></td>
                      <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                      <td>
                        <?php
                        switch ($pembayaran['bulan_bayar']) {
                          case "1";
                            $bulan_bayar = "Januari";
                          break;
                          case "2";
                            $bulan_bayar = "Februari";
                          break;
                          case "3";
                            $bulan_bayar = "Maret";
                          break;
                          case "4";
                            $bulan_bayar = "April";
                          break;
                          case "5";
                            $bulan_bayar = "Mei";
                          break;
                          case "6";
                            $bulan_bayar = "Juni";
                          break;
                          case "7";
                            $bulan_bayar = "Juli";
                          break;
                          case "8";
                            $bulan_bayar = "Agustus";
                          break;
                          case "9";
                            $bulan_bayar = "September";
                          break;
                          case "10";
                            $bulan_bayar = "Oktober";
                          break;
                          case "11";
                            $bulan_bayar = "November";
                          break;
                          case "12";
                            $bulan_bayar = "Desember";
                          break;
                        }
                        echo $bulan_bayar; ?>
                      </td>
                      <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                      <td><?php echo $pembayaran['biaya_admin']; ?></td>
                      <td><?php echo $pembayaran['total_bayar']; ?></td>
                      <td>
                        <a href="hapus_pembayaran.php?id_pembayaran=<?php echo $pembayaran['id_pembayaran']; ?>"><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>
                        <a href="cetak_struk.php"><button type="button" class="btn btn-warning"> <span class="glyphicon glyphicon-print"></span> Cetak</button></a>
                      </td>
                    </tr>

                  <?php } ?>

                </tbody>
              </table>

            </div>
          </div>

         </div>
       </div>
       <!-- /. ROW  -->
       <hr />

     </div>
     <!-- /. PAGE INNER  -->
   </div>
   <!-- /. PAGE WRAPPER  -->
 </div>
 <!-- /. WRAPPER  -->
 <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
 <!-- JQUERY SCRIPTS -->
 <script src="../assets/js/jquery-1.10.2.js"></script>
 <!-- BOOTSTRAP SCRIPTS -->
 <script src="../assets/js/bootstrap.min.js"></script>
 <!-- METISMENU SCRIPTS -->
 <script src="../assets/js/jquery.metisMenu.js"></script>
 <!-- DATA TABLE SCRIPTS -->
<script src="../assets/js/dataTables/jquery.dataTables.js"></script>
<script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- CUSTOM SCRIPTS -->
<script>
  $(document).ready(function () {
    $('#example').dataTable();
  });
</script>
<script src="../assets/js/custom.js"></script>
<script>
  function print_d(){
    window.open("print_laporan_pembayaran.php","_blank")
  }
</script>


</body>
</html>
