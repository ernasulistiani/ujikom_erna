<?php
include 'koneksi.php'; 
session_start();
if (empty($_SESSION['username'])) {
  header('location:login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
  $pelanggan = mysqli_fetch_array($query_pelanggan);
}
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>E-PLN</title>
  <!-- BOOTSTRAP STYLES-->
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/js/dataTables/dataTables.bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">E-PLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout <span class="glyphicon glyphicon-log-out"></span></a> </div>
    </nav> 

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="img/log.png" class="user-image img-responsive"/>
            <p style="color: white; margin-top: -25px"><?php echo $pelanggan['nama_pelanggan'];?></p>
          </li>
          <li>
            <a href="index.php"><i class="fa fa-home fa-2x"></i>Dashboard</a>
          </li>
          <li>
            <a href="saldo.php"><i class="fa fa-usd fa-2x"></i>Saldo</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Riwayat<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="riwayat_pembayaran.php">Pembayaran</a>
              </li>
              <li>
                <a href="riwayat_tagihan.php">Tagihan</a>
              </li>
              <li>
                <a href="riwayat_penggunaan.php">Penggunaan</a>
              </li>
              <li>
                <a href="riwayat_topup_saldo.php">Top-Up Saldo</a>
              </li>
            </ul>
          </li>     
        </ul>

      </div>
      <?php
      include "koneksi.php";
$today = date("Ymd"); //untuk mengambil tahun, tanggal dan tahun hari ini
$tanggal_daftar = date("d/m/Y");
//cari id terakhir di tanggal hari ini
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];

//setelah membaca id terakhir , lanjut mencari nomor urut id dari id terakhir
$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++; //nomor urut +1

//setelah ketemu id terakhir lanjut membuat id baru dengan format sbb:
$NewID = $today .sprintf('%03s',$NoUrut);
//$today nanti jadinya misal 20160526 .sprintf('%03s', $NoUrut) urutan id di tanggal hari ini


?>
</nav>  
<!-- /. NAV SIDE  -->
<div id="page-wrapper" >
  <div id="page-inner">
    <div class="row">
      <div class="col-md-12">  
       <h4 align="center">Riwayat Tagihan</h4>
       <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" id="example">
            <thead>
              <tr>
                <th>ID Tagihan</th>
                <th>Bulan</th>
                <th>Tahun</th>
                <th>Jumlah Penggunaan</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php

              include 'koneksi.php';
              $query_tagihan = mysqli_query($koneksi, "SELECT * FROM tagihan WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
              while ($tagihan=mysqli_fetch_array($query_tagihan)) {
                $id_pelanggan = $_SESSION['id_pelanggan'];
                $query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' and bulan='$tagihan[bulan]'");
                $penggunaan = mysqli_fetch_array($query_penggunaan);
                $id_penggunaan = $penggunaan['id_penggunaan'];

//Penggambilan data di tabel pelanggan
                $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
                $pelanggan = mysqli_fetch_array($query_pelanggan);
                $nama_pelanggan = $pelanggan['nama_pelanggan'];
                $id_tarif = $pelanggan['id_tarif'];

//pengambilan data di tabel tarif berdasarkan id tarif pelanggan
                $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                $tarif = mysqli_fetch_array($query_tarif);
                $tarifperkwh = $tarif['tarifperkwh'];

                $bulan_tagihan = $penggunaan['bulan'];
                $tahun_tagihan = $penggunaan['tahun'];
                $jumlah_meter_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal'];
                $jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh;
                $tahun_cek = date("Y");
                $bulan_cek = date('n');
                $tanggal_denda = date('d');
                if ($tanggal_denda > 20){
                  $jumlah_denda = 5000;
                }else {
                  $jumlah_denda = 0;
                } ?>
                <tr>
                  <td><?php echo $tagihan['id_tagihan']; ?></td>
                  <td>
                    <?php
                    $id_tagihan = $tagihan['id_tagihan'];
                    switch ($tagihan['bulan']) {
                      case "1";
                      $bulan = "Januari";
                      break;
                      case "2";
                      $bulan = "Februari";
                      break;
                      case "3";
                      $bulan = "Maret";
                      break;
                      case "4";
                      $bulan = "April";
                      break;
                      case "5";
                      $bulan = "Mei";
                      break;
                      case "6";
                      $bulan = "Juni";
                      break;
                      case "7";
                      $bulan = "Juli";
                      break;
                      case "8";
                      $bulan = "Agustus";
                      break;
                      case "9";
                      $bulan = "September";
                      break;
                      case "10";
                      $bulan = "Oktober";
                      break;
                      case "11";
                      $bulan = "November";
                      break;
                      case "12";
                      $bulan = "Desember";
                      break;
                    }
                    echo $bulan; ?>
                  </td>
                  <td><?php echo $tagihan['tahun']; ?></td>
                  <td><?php echo $tagihan['jumlah_meter']; ?></td>
                  <td><?php 
                  if ($tagihan['status'] == "Lunas") {
                    echo "<button class='btn btn-success btn-sm'>".$tagihan['status']."</button>";
                  }
                  else {
                    echo "<button data-toggle='modal' data-target='#bayar$id_tagihan'class='btn btn-danger btn-sm'>".$tagihan['status']."</button>";
                  }
                  ?></td>
                </tr>

                <!-- Modal -->
                <div class="modal fade" id="bayar<?php echo $id_tagihan;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Riwayat Tagihan yang belum Dibayar<?php echo $id_tagihan; ?></h4>
                      </div>
                      <div class="modal-body">
                        <form action="proses_pembayaran.php?p=<?php echo $id_tagihan; ?>" method="POST">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                            <div class="col-sm-9">
                              <input type="text" name="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nomor KWH</label>
                            <div class="col-sm-9">
                              <input type="text" name="nomor_kwh" class="form-control" value="<?php echo $pelanggan['nomor_kwh']; ?>" placeholder="Nomor kwh" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Daya (Watt)</label>
                            <div class="col-sm-9">
                              <input type="number" name="daya" class="form-control" placeholder="Daya" value="<?php echo $tarif['daya']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tarif Per-Kwh</label>
                            <div class="col-sm-9">
                              <input type="number" name="tarifperkwh" class="form-control" placeholder="Bulan Bayar" value="<?php echo $tarif['tarifperkwh']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Penggunaan</label>
                            <div class="col-sm-9">
                              <input type="number" name="jumlah_penggunaan" class="form-control" placeholder="Jumlah Bayar" value="<?php $jumlah = $penggunaan['meter_akhir'] - $penggunaan['meter_awal']; echo $jumlah; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                            <div class="col-sm-9">
                              <input type="text" name="jumlah_tagihan" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_tagihan; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Biaya Admin</label>
                            <div class="col-sm-9">
                              <input type="text" name="biaya_admin" class="form-control"  placeholder="Total Bayar" value="2500" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Denda</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_denda; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Total Bayar</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php $jumlah_bayar=$jumlah_tagihan + 2500 + $jumlah_denda; echo $jumlah_bayar; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                              <input type="text" name="status" class="form-control"  placeholder="Total Bayar" value="<?php echo $tagihan['status']; ?>" readonly>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <input type="submit" class="btn btn-danger" name="bayar" value="Bayar">
                          <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <?php } ?>

              </tbody>
            </table>
          </div>
        </div>

      </div>

    </div>
    <!-- /. ROW  -->
    <hr />




  </div>
  <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="assets/js/dataTables/jquery.dataTables.js"></script>
<script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
<!-- CUSTOM SCRIPTS -->
<script>
  $(document).ready(function () {
    $('#example').dataTable();
  });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>


</body>
</html>
